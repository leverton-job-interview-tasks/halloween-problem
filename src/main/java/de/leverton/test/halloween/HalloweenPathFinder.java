/* Copyright (c) 2017 Leverton GmbH - all rights reserved
 * Unauthorized reproduction, copying, modification, distribution or 
 * other use of this file via any medium is strictly prohibited.
 *  
 * Proprietary and confidential.
 *  
 * Written by Marc Ewert <marc.ewert@leverton.ai>, 2017-11-01
 */

package de.leverton.test.halloween;

import de.leverton.test.halloween.model.Step;

import java.util.Collections;
import java.util.List;

/**
 * Finds a solution for the problem described in README.md.
 */
class HalloweenPathFinder {

    /**
     * Creates a new instance of the problem solver. Please don't change the parameter list.
     * @param housesJson JSON containing the houses of the problem. See src/main/resources/houses.json for an example.
     * @param pathsJson JSON containing the paths between the houses. See src/main/resources/paths.json for an example.
     */
    HalloweenPathFinder(String housesJson, String pathsJson) {
        // TODO Replace with implementation
    }

    /**
     * Actually returns the solution for the given starting point and maximal distance to walk.
     * Please don't change the parameter list.
     * @param originId Defines the house to start the walk.
     * @param maxDistance Maximal distance in meters to walk.
     * @return Returns the steps to walk, for getting the most sweets in the shortest distance.
     */
    List<Step> findPerfectRoute(String originId, double maxDistance) {
        return Collections.emptyList(); // TODO Replace with implementation
    }
}
